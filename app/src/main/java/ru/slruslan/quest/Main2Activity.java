package ru.slruslan.quest;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.slruslan.quest.Situations.Situation;
import ru.slruslan.quest.Stories.Story1.Story1;

public class Main2Activity extends AppCompatActivity implements View.OnClickListener {

    TextView question;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        question = (TextView)findViewById(R.id.textView2);

        intent = getIntent();
        Situation s = (Situation)intent.getExtras().get("s");

        if(s != null)
            question.setText(s.getText());

        LinearLayout ll = (LinearLayout)findViewById(R.id.optionsLayout);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        for(int j = 0; j < s.getDirectionsCount(); j++) {
            final DataButton myButton = new DataButton(this);
            myButton.setText(String.valueOf(j+1));
            myButton.addData("answerId", j);
            myButton.setOnClickListener(this);

            ll.addView(myButton, lp);
        }

        if(!s.hasDirections()) {
            final DataButton myButton = new DataButton(this);
            myButton.setText("Закончить");
            myButton.addData("answerId", -1);
            myButton.setOnClickListener(this);

            ll.addView(myButton, lp);
        }
    }


    @Override
    public void onClick(View v) {
        DataButton button = (DataButton)v;

        intent.putExtra("answerId", button.getData("answerId").toString());
        setResult(200, intent);
        finish();
    }
}
