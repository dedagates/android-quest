package ru.slruslan.quest;

/**
 * Represents game character
 */
public class Character {
    private String name;
    private int K, A, P;

    public Character(String name, int K, int A, int P) {
        this.K = K;
        this.A = A;
        this.P = P;
        this.name = name;
    }

    public Character(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getK() {
        return K;
    }

    public void setK(int k) {
        K = k;
    }

    public int getA() {
        return A;
    }

    public void setA(int a) {
        A = a;
    }

    public int getP() {
        return P;
    }

    public void setP(int p) {
        P = p;
    }
}
