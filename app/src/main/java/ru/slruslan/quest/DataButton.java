package ru.slruslan.quest;

import android.content.Context;
import android.widget.Button;

import java.util.ArrayList;
import java.util.HashMap;

public class DataButton extends Button {

    private HashMap<String, Object> buttonData;

    public DataButton(Context context) {
        super(context);

        this.buttonData = new HashMap<String, Object>();
    }

    public void addData(String key, Object value) {
        this.buttonData.put(key, value);
    }

    public Object getData(String key) {
        return this.buttonData.get(key);
    }
}
