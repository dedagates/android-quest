package ru.slruslan.quest;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import ru.slruslan.quest.Situations.Situation;
import ru.slruslan.quest.Stories.Story;
import ru.slruslan.quest.Stories.Story1.Story1;

public class MainActivity extends AppCompatActivity {

    EditText name;
    QuestGame game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        name = (EditText)findViewById(R.id.editText);

        final DataButton myButton = new DataButton(this);
        myButton.setText(Story1.NAME);
        myButton.addData("storyId", "0");

        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    validateName();
                    Character character = new ru.slruslan.quest.Character(name.getText().toString());
                    Story story;

                    switch(myButton.getData("storyId").toString()) {
                        case "0":
                            story = new Story1();
                        break;

                        default:
                            Log.d("QUEST", "Non-existing story selected. Skipping");
                        return;
                    }

                    game = new QuestGame(story, character);
                    showQuestion(game.getCurrentSituation());
                } catch (ValidationException e) {
                    Toast.makeText(getApplicationContext(), "Пожалуйста, введите имя", Toast.LENGTH_LONG).show();
                }

            }
        });

        LinearLayout ll = (LinearLayout)findViewById(R.id.buttonLayout);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        ll.addView(myButton, lp);
    }

    private void validateName() throws ValidationException {
        if(name.getText().toString().equals(""))
            throw new ValidationException();
    }

    private void showQuestion(Situation sit) {
        Intent intent = new Intent(getBaseContext(), Main2Activity.class);
        intent.putExtra("s", sit);

        startActivityForResult(intent, 200);
    }

    private void showFinal() {
        Intent intent = new Intent(getBaseContext(), FinalActivity.class);
        intent.putExtra("P", game.getCharacter().getP());
        intent.putExtra("K", game.getCharacter().getK());
        intent.putExtra("A", game.getCharacter().getA());

        startActivity(intent);

        game.nextSituation(0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Situation sit = game.getCurrentSituation();

        int answer = Integer.parseInt(data.getStringExtra("answerId"));
        Log.d("QUEST", "We got answer "+answer);

        if(answer != -1) {
            showQuestion(game.nextSituation(answer));
        } else {
            showFinal();
        }
    }
}
