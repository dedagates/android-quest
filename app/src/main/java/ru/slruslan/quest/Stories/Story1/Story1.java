package ru.slruslan.quest.Stories.Story1;

import ru.slruslan.quest.Situations.Story1.SituationWindows;
import ru.slruslan.quest.Stories.Story;

public class Story1 extends Story {
    public static String NAME = "История 1";
    public static String DESCRIPTION = "Первая история в квесте";

    public Story1() {
        this.currentSituation = new SituationWindows();
    }
}
