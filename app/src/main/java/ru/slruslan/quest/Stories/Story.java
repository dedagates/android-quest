package ru.slruslan.quest.Stories;

import ru.slruslan.quest.Situations.Situation;

public abstract class Story {
    public static String NAME = "История";
    public static String DESCRIPTION = "Описание";

    public Situation getCurrentSituation() {
        return currentSituation;
    }

    protected Situation currentSituation;

    protected int lastdK;
    protected int lastdA;
    protected int lastdP;

    public int getLastdP() {
        return lastdP;
    }

    public int getLastdK() {
        return lastdK;
    }

    public int getLastdA() {
        return lastdA;
    }

    public Situation nextSituation(int answerId) {
        this.currentSituation = currentSituation.doAnswer(answerId);

        if(this.currentSituation == null)
            return null;

        this.lastdP = this.currentSituation.getdP();
        this.lastdK = this.currentSituation.getdK();
        this.lastdA = this.currentSituation.getdA();

        return currentSituation;
    }
}