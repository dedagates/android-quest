package ru.slruslan.quest;

import java.lang.*;

import ru.slruslan.quest.Situations.Situation;
import ru.slruslan.quest.Stories.Story;

public class QuestGame {

    ru.slruslan.quest.Character character;
    Story story;

    public QuestGame(Story story, ru.slruslan.quest.Character character) {
        this.character = character;
        this.story = story;
    }

    public Situation nextSituation(int answerId) {
        Situation sit =  this.story.nextSituation(answerId);

        this.character.setA(this.character.getA() + this.story.getLastdA());
        this.character.setP(this.character.getP() + this.story.getLastdP());
        this.character.setK(this.character.getK() + this.story.getLastdK());

        return sit;
    }

    public Situation getCurrentSituation() {
        return this.story.getCurrentSituation();
    }

    public ru.slruslan.quest.Character getCharacter() {
        return this.character;
    }
}
