package ru.slruslan.quest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class FinalActivity extends AppCompatActivity {

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final);

        text = (TextView)findViewById(R.id.textView4);

        text.setText(String.format("Вы набрали:\nP: %s, K: %s, A: %s",
                getIntent().getExtras().get("P"),
                getIntent().getExtras().get("K"),
                getIntent().getExtras().get("A")));
    }
}
