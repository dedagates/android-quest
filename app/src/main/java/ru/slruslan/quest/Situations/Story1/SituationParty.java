package ru.slruslan.quest.Situations.Story1;

import ru.slruslan.quest.Situations.Situation;

public class SituationParty extends Situation {
    public SituationParty() {
        this.subject = "Корпоратив";
        this.text = "\"Неудачное начало, ну чтож, сегодня в конторе копроратив! "
                + "Познакомлюсь с коллегами, людей так сказать посмотрю, себя покажу";

        this.dA = 0;
        this.dK = -10;
        this.dP = -10;
    }
}
