package ru.slruslan.quest.Situations.Story1;

import ru.slruslan.quest.Situations.Situation;

public class SituationMeeting extends Situation {
    public SituationMeeting() {
        this.subject = "Совещание";
        this.text = "\"Сегодня будет совещание, меня неожиданно вызвали, "
                + "есть сведения что \n босс доволен сегодняшней сделкой";

        this.dA = 1;
        this.dK = 100;
        this.dP = 0;
    }
}
