package ru.slruslan.quest.Situations.Story1;

import ru.slruslan.quest.Situations.Situation;

public class SituationWindows extends Situation {
    public SituationWindows() {
        this.subject = "Первая сделка (Windows)";
        this.text = "Только вы начали работать и тут-же удача! Вы нашли клиента и продаете ему "
                + "партию ПО MS Windows. Ему в принципе достаточно взять 100 коробок версии Home\n"
                + "(1)у клиента денег много, а у меня нет - вы выпишете ему счет на 120 коробок ULTIMATE по 50тр\n"
                + "(2)чуть дороже сделаем, кто там заметит - вы выпишете ему счет на 100 коробок PRO по 10тр\n"
                + "(3)как надо так и сделаем - вы выпишете ему счет на 100 коробок HOME по 5тр";

        this.dA = 0;
        this.dK = 0;
        this.dP = 0;

        this.direction = new Situation[]{ new SituationParty(), new SituationMeeting(), new SituationClientIsLoyal()};
    }
}
