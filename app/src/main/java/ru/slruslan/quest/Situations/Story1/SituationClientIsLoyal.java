package ru.slruslan.quest.Situations.Story1;

import ru.slruslan.quest.Situations.Situation;

public class SituationClientIsLoyal extends Situation {
    public SituationClientIsLoyal() {
        this.subject = "Мой первый лояльный клиент";
        this.text = "Мой первый клиент доволен скоростью и качеством моей работы. "
                + "Сейчас мне звонил лично директор компании и сообщил, что скоро состоится еще более крупная сделка и он хотел, чтобы по ней работал именно я!";

        this.dA = 0;
        this.dK = 50;
        this.dP = 1;
    }
}
