package ru.slruslan.quest.Situations;

import java.io.Serializable;

public class Situation implements Serializable {
    protected Situation[] direction;
    protected String subject, text;
    protected int dK, dA, dP;

    public String getSubject() {
        return subject;
    }

    public String getText() {
        return text;
    }

    public int getdK() {
        return dK;
    }

    public int getdA() {
        return dA;
    }

    public int getdP() {
        return dP;
    }

    public Situation doAnswer(int answerId) {
        if(direction == null || answerId >= direction.length)
            return null;

       return direction[answerId];
    }

    public boolean hasDirections() {
        return this.direction != null;
    }

    public int getDirectionsCount() { return (this.direction != null) ? this.direction.length : 0; };
}
